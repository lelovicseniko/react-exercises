import { useEffect, useState } from "react";
import { useMyState } from "./hooks/useMyState";
import "./styles.css";

export default function App() {
  const [a, setA] = useState(0);
  const [b, setB] = useMyState(0);

  useEffect(() => {
    //setA(2);
    //setB(2);
    console.log('add')
    setA((elem) => elem + 1);
    setB((elem) => elem + 1);
  }, []);

  useEffect(() => {
    console.log(a, b);
  }, [a, b]);

  return (
    <div className="App">
      <h1>Hello CodeSandbox</h1>
      <h2>Start editing to see some magic happen!</h2>
    </div>
  );
}
