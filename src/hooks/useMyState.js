import { useReducer } from "react";

/*
Challenge: write a custom hook myUseState() that works exactly
useState(), only that it uses the useReducer() hook to manage
the state.
*/

export function useMyState(initial) {
  const [state, dispatch] = useReducer(reducer, initial);
  return [state, dispatch];
}

function reducer(state, lambdaOrValue) {
  if (typeof lambdaOrValue !== "function") {
    return lambdaOrValue;
  } else {
    return lambdaOrValue(state);
  }
}
