Hooks
=====

useMyState
----------

Challenge: write a custom hook myUseState() that works exactly
useState(), only that it uses the useReducer() hook to manage
the state.
